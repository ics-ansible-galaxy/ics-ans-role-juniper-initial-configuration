ics-ans-role-juniper-initial-configuration
==========================================

Ansible role to generate a basic juniper configuration based on csentry data.



Requirements
------------

- ansible >= 2.7
- molecule >= 2.20

Role Variables
--------------

```yaml
juniper_initial_configuration_encrypted_password: fake-pass-replace-with-encrypted-in-csentry
juniper_initial_configuration_user: replace_in_csentry
juniper_initial_configuration_user_password: replace_in_csentry
juniper_initial_configuration_user_id: 2001
juniper_initial_configuration_disable: disable
juniper_initial_configuration_protocol_version: v2
juniper_initial_configuration_forwarding_options: dhcp-security
juniper_initial_configuration_name_servers:
  - 172.16.2.21
  - 172.16.2.22
juniper_initial_configuration_routing_options_route: 0.0.0.0/0
juniper_initial_configuration_slax_script: https://194.47.240.184/artifactory/swi-pkg/juniper/scripts/fetch-fw.slax
juniper_initial_configuration_inband: true

```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-juniper-initial-configuration
```

License
-------

BSD 2-clause
