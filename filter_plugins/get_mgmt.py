def check_mgmt(item):
    for interface in item.get("csentry_interfaces"):
        if contains_mgmt(interface):
            return True
    return False


def contains_mgmt(interface):
    if "mgmt" in interface.get("network").get("name").lower():
        return True
    return False


class FilterModule(object):
    def filters(self):
        return {"has_mgmt": check_mgmt}
